<?php

namespace Modules\Common\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(\Modules\Common\Repositories\RewardRepository::class, \Modules\Common\Repositories\RewardRepositoryEloquent::class);
        $this->app->bind(\Modules\Common\Repositories\MenuRepository::class, \Modules\Common\Repositories\MenuRepositoryEloquent::class);
        $this->app->bind(\Modules\Common\Repositories\PostRepository::class, \Modules\Common\Repositories\PostRepositoryEloquent::class);
        //:end-bindings:
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
