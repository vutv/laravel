<?php

namespace Modules\Common\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\APIController;
use Modules\Common\Repositories\RewardRepository;

class RewardController extends APIController
{
    private $rewardRepository;

    public function __construct( RewardRepository $rewardRepository)
    {
        $this->rewardRepository = $rewardRepository;
    }

    public function index()
    {
        $itemAll = $this->rewardRepository->getAllReward();
//        dd($itemAll);
        return $this->responseData([
            'data' => [
                'listItem' => $itemAll['data'],
                'pagination' => $itemAll['meta']['pagination']
            ]
        ]);
    }

    public function store(Request $request)
    {
        $params = $request->all();
        $this->rewardRepository->add($params);
        return $this->responseData(['data' => 'ok']);
    }

    public function show($id)
    {
        $jobDetail = $this->rewardRepository->getRewardById($id);
        if (empty($jobDetail['data'])) {
            return $this->responseError(__('reward not found'));
        }
        return $this->responseData(['data' => $jobDetail['data']]);
    }

    public function update(Request $request)
    {
        $params = $request->all();
        unset($params['id']);
        $id = $request->id;
        $this->rewardRepository->update($params, $id);
        return $this->responseData(['data' => $id]);
    }

    public function destroy($id)
    {
        $jobDetail = $this->rewardRepository->getRewardById($id);
        if (empty($jobDetail['data'])) {
            return $this->responseError(__('reward not found'));
        }

        $this->rewardRepository->remove($id);
        return $this->responseData(['data' => 'ok']);
    }
}
