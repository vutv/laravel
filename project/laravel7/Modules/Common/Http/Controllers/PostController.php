<?php

namespace Modules\Common\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\APIController;
use Illuminate\Support\Facades\Log;
use Modules\Common\Repositories\PostRepository;
use App\Repositories\ES\PostElasticRepository as Client;

class PostController extends APIController
{
    private $postRepository;
    private $client;

    public function __construct(PostRepository $postRepository)
    {
        parent::__construct();
        $this->postRepository = $postRepository;
        $this->client = app(Client::class);
    }

    public function index(Request $request)
    {
        $conditions = $request->all();
        $results = $this->client->search($conditions);
        Log::debug($results);
//        $this->protectedData($results['listItem']);
        return $this->responseData($results);
    }

    public function store(Request $request)
    {
        $params = $request->all();
        $this->postRepository->add($params);
        return $this->responseData(['data' => 'ok']);
    }

    public function show($id)
    {
        $post = $this->postRepository->getById($id);
        return $this->responseData($post);
    }

    public function update($id)
    {
        return $this->responseData(['data' => $id]);
    }

    public function destroy($id)
    {
        $this->postRepository->remove($id);
        return $this->responseData(['data' => $id]);
    }
}
