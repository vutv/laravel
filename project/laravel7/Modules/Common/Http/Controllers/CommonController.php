<?php

namespace Modules\Common\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\APIController;

class CommonController extends APIController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        return $this->responseData(['data' => 'ok']);
    }

    public function store()
    {
        return $this->responseData(['data' => 'ok']);
    }

    public function show($id)
    {
        return $this->responseData(['data' => $id]);
    }

    public function update($id)
    {
        return $this->responseData(['data' => $id]);
    }

    public function destroy($id)
    {
        return $this->responseData(['data' => $id]);
    }
}
