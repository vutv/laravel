<?php

use Dingo\Api\Routing\Router;

$api = app(Router::class);
$routeConfig = [
    'middleware' => config('modules.api_middleware'),
    'namespace' => 'Modules\Common\Http\Controllers',
    'prefix' => '/api/common'
];

$api->version('v1', $routeConfig,
    function (Router $api) {
        $api->get('/', 'CommonController@index');
        $api->get('/reward', 'RewardController@index');
        $api->post('/reward/create', 'RewardController@store');
        $api->post('/reward/edit', 'RewardController@update');
        $api->get('/reward/{id}', 'RewardController@show');
        $api->put('/reward/{id}/delete', 'RewardController@destroy');

        $api->get('/post', 'PostController@index');
        $api->post('/post/create', 'PostController@store');
        $api->post('/post/edit', 'PostController@update');
        $api->get('/post/{id}', 'PostController@show');
        $api->put('/post/{id}/delete', 'PostController@destroy');
    }
);
