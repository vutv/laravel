<?php

namespace Modules\Common\Repositories;

use App\Repositories\MyRepository;

/**
 * Interface PostElasticRepository.
 *
 * @package namespace Modules\Common\Repositories;
 */
interface PostRepository extends MyRepository
{
    //
    public function getPostByListId($ids = []);
}
