<?php

namespace Modules\Common\Repositories;

use App\Repositories\MyRepository;

/**
 * Interface RewardRepository.
 *
 * @package namespace Modules\Common\Repositories;
 */
interface RewardRepository extends MyRepository
{
    //
    public function getAllReward();

    public function getRewardById($id);
}
