<?php

namespace Modules\Common\Repositories;

use App\Repositories\MyRepositoryEloquent;
use App\Entities\Reward;
use App\Presenters\RewardPresenter;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class RewardRepositoryEloquent.
 *
 * @package namespace Modules\Common\Repositories;
 */
class RewardRepositoryEloquent extends MyRepositoryEloquent implements RewardRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Reward::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->setPresenter(app(RewardPresenter::class));
    }


    /**
     * @return mixed
     */
    public function getAllReward()
    {
        return $this->paginate();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getRewardById($id)
    {
        $relations = [];
        $model = $this->with($relations);
        if ($model instanceof RewardRepositoryEloquent) {
            return $model->getFirst(['id' => $id]);
        }
        return [];
    }
}
