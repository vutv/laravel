<?php

namespace Modules\Common\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MenuRepository.
 *
 * @package namespace Modules\Common\Repositories;
 */
interface MenuRepository extends RepositoryInterface
{
    //
}
