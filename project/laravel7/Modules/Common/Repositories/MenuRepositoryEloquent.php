<?php

namespace Modules\Common\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Common\Repositories\menuRepository;
use Modules\Common\Entities\Menu;
use Modules\Common\Validators\MenuValidator;

/**
 * Class MenuRepositoryEloquent.
 *
 * @package namespace Modules\Common\Repositories;
 */
class MenuRepositoryEloquent extends BaseRepository implements MenuRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Menu::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
