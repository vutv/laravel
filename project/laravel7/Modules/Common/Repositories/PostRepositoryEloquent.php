<?php

namespace Modules\Common\Repositories;

use App\Repositories\MyRepositoryEloquent;
use App\Entities\Post;
use App\Presenters\PostPresenter;
use App\Trigger\PostTrigger;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class PostRepositoryEloquent.
 *
 * @package namespace Modules\Common\Repositories;
 */
class PostRepositoryEloquent extends MyRepositoryEloquent implements PostRepository
{
    use PostTrigger;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Post::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->setPresenter(app(PostPresenter::class));
    }



    public function getPostByListId($ids = [])
    {
        if (empty($ids)) {
            return [];
        }
        return $this->scopeQuery(function ($query) use ($ids) {
            return $query->whereIn('id', $ids);
        })->all();
    }
}
