<?php

namespace App\Repositories\ES;

interface PostElasticRepository
{
    public function search(array $conditions);

    public function indexAllPost();

    public function sync(array $ids);
}
