<?php

namespace App\Repositories\ES;

use Elasticsearch\ClientBuilder;

abstract class MyElasticSearch
{
    /**
     * @var \Elasticsearch\Client
     */
    protected $client;

    protected $type = null;

    protected $index = null;


    public function __construct()
    {
        $hosts = config('elasticsearch.hosts');
        $this->client = ClientBuilder::create()->setHosts($hosts)->build();
    }

    protected function truncate()
    {
        $params = [
            'index' => $this->index
        ];
        try {
            $this->client->indices()->delete($params);
        } catch (\Exception $e) {
            app('log')->debug($e->getMessage());
        } finally {
            if ($this->index == config('elasticsearch.job.index')) {
                $params['body'] = [
                    'mappings' => [
                        '_default_' => [
                            'properties' => [
                                'updated_at' => [
                                    'type' => 'date',
                                    'format' => 'yyyy-MM-dd H:m:s'
                                ]
                            ]
                        ]
                    ]
                ];
            }
            $this->client->indices()->create($params);
        }
    }

    protected function index($document = [])
    {
        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'id' => $document['id'],
            'body' => $document
        ];
        $this->client->index($params);
    }

    protected function delete($documentId)
    {
        try {
            $params = [
                'index' => $this->index,
                'type' => $this->type,
                'id' => $documentId
            ];
            $this->client->delete($params);
        } catch (\Exception $ex) {
            app('log')->debug($ex->getMessage());
        }
    }

    protected function parserResult($result, callable $callback = null)
    {
        $response = [
            'listItem' => [],
            'pagination' => [
                'per_page' => config('repository.pagination.limit'),
                'total_item' => 0,
                'total_page' => 0
            ],
            'filter' => []
        ];

        if ($result['hits']['total'] == 0) {
            return $response;
        }

        foreach ($result['hits']['hits'] as $item) {
            if ($callback) {
                $item = call_user_func($callback, $item['_source']);
            } else {
                $item = $item['_source'];
            }
            $response['listItem'][] = $item;
        }

        if (!empty($result['aggregations'])) {
            $filter = [];
            foreach ($result['aggregations']['all'] as $field => $agg) {
                if ($field == 'doc_count') {
                    continue;
                }

                if (empty($agg[$field]['buckets'])) {
                    continue;
                }

                $filter[$field] = collect($agg[$field]['buckets'])
                    ->map(function ($item) {
                        return ['key' => $item['key'], 'count' => $item['doc_count']];
                    })
                    ->sortByDesc('doc_count')
                    ->all();
            }
            $response['filter'] = $filter;
        }
        $totalPage = ceil($result['hits']['total']['value'] / config('repository.pagination.limit'));
        $response['pagination'] = [
            'per_page' => config('repository.pagination.limit'),
            'total_item' => $result['hits']['total']['value'],
            'total_page' => $totalPage
        ];
        return $response;
    }

    protected function filterData($data)
    {
        if (empty($data)) {
            return [];
        }
        return array_values(array_unique(array_filter($data)));
    }
}
