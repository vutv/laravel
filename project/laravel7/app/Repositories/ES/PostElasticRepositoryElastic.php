<?php

namespace App\Repositories\ES;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Str;
use Modules\Common\Repositories\PostRepository as Post;
use stdClass;

class PostElasticRepositoryElastic extends MyElasticSearch implements PostElasticRepository
{
    /**
     * @var Job
     */
    private $postRepository;

    public function __construct()
    {
        parent::__construct();
        $this->type = config('elasticsearch.post.type');
        $this->index = config('elasticsearch.post.index');

        $this->postRepository = app(Post::class);
    }

    private function documentStructure()
    {
        return [
            'id' => '',
            'tag' => [],
            'title' => '',
            'content' => '',
            'updated_at' => now(),
            'sort_date' => time(),
        ];
    }

    private function processDocument($post = [])
    {
        if (empty($post)) {
            return false;
        }

        $document = $this->documentStructure();

        $document['id'] = $post['id'];
        $document['tag'] = explode(',', $post['tag']);
        $document['title'] = $post['title'];
        $document['content'] = $post['content'];
        $document['updated_at'] = $post['updated_at'];
        $document['sort_date'] = Carbon::parse($post['updated_at'])->timestamp;

        $this->index($document);

        return true;
    }

    public function indexAllPost()
    {
        $this->truncate();
        $totalItem = 0;

        $result = $this->postRepository->getAll();
        if (empty($result['data'])) {
            app('log')->debug('post not found');
            return [
                'status' => 0,
                'message' => 'post not found'
            ];
        }

        foreach ($result['data'] as $post) {
            if ($this->processDocument($post)) {
                $totalItem++;
            }
        }

        app('log')->debug('Total item: '.$totalItem);
        return [
            'status' => 1,
            'message' => 'Total item: '.$totalItem
        ];
    }

    public function search(array $conditions)
    {
        $query = [];
        $page = $conditions['page'] ?? 0;
        if ($page <= 1) {
            $page = 1;
        }
        $conditions = array_filter($conditions);
        if (!empty($conditions['page'])) {
            unset($conditions['page']);
        }
        $params = [
            'index' => config('elasticsearch.post.index'),
            'type' => config('elasticsearch.post.type'),
            'body' => [
                'query' => [
                    'bool' => [
                        'must' => []
                    ]
                ],
                'from' => ($page - 1) * config('repository.pagination.limit'),
                'size' => config('repository.pagination.limit')
            ]
        ];

        if (empty($conditions)) {
            $params['body']['sort'] = [
                'updated_at' => 'desc'
            ];
        }
        if (!empty($conditions['text'])) {
            if (Str::length($conditions['text']) == 1) {
                $query['must'][]['match_phrase_prefix']['text'] =
                    $conditions['text'];
            } else {
                $query['must'][]['multi_match'] = [
                    'query' => $conditions['text'],
                    'fields' => [
                        'title^3',
                        'tag^2',
                        'content',
                    ]
                ];
            }
        }
        if (!empty($conditions['sort']) && $conditions['sort'] == 'day') {
            $params['body']['sort'] = [
                'sort_date' => 'desc'
            ];
        }

        if (!empty($query)) {
            $params['body']['query']['bool'] = $query;
        }
        if (!empty($params['body']['query']['bool']['must'])) {
            $params['body']['query']['bool']['must'] = array_values($params['body']['query']['bool']['must']);
        }

        app('log')->debug(json_encode($params));
        $result = $this->client->search($params);
        app('log')->info($result);
        return $this->parserResult($result, function ($item) {
            return $item;
        });
    }

    public function sync(array $ids)
    {
        $result = $this->postRepository->getPostByListId($ids);
        if (empty($result['data'])) {
            $documentNeedDelete = $ids;
        } else {
            foreach ($result['data'] as $job) {
                $this->processDocument($job);
            }
            $documentNeedDelete = array_diff($ids, array_column($result['data'], 'id'));
        }

        if (!empty($documentNeedDelete)) {
            $this->deleteDocument($documentNeedDelete);
        }
    }

    private function deleteDocument($ids = [])
    {
        foreach ($ids as $id) {
            $this->delete($id);
        }
    }
}
