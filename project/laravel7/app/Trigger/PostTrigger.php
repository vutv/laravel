<?php

namespace App\Trigger;

use App\Jobs\SyncPostWithElasticSearch;

trait PostTrigger
{
    /**
     * @param string|array|\Closure $id
     */
    private function syncElasticSearch($id)
    {
        if (!is_array($id)) {
            $id = [$id];
        }
        SyncPostWithElasticSearch::dispatch($id)->onQueue('sync_job');
    }

    public function create(array $attributes)
    {
        $response = parent::create($attributes);
        app('log')->debug('PostTrigger: create');

        $primaryKey = $this->model->getKeyName();
        $this->syncElasticSearch($response['data'][$primaryKey]);
        return $response;
    }

    public function update(array $attributes, $id)
    {
        $response = parent::update($attributes, $id);
        app('log')->debug('PostTrigger: update --- ' . $id);

        $this->syncElasticSearch($id);
        return $response;
    }

    public function updateOrCreate(array $attributes, array $values = [])
    {
        $response = parent::updateOrCreate($attributes, $values);
        app('log')->debug('PostTrigger: updateOrCreate');

        $primaryKey = $this->model->getKeyName();
        $this->syncElasticSearch($response['data'][$primaryKey]);
        return $response;
    }

    public function delete($id)
    {
        $response = parent::delete($id);
        app('log')->debug('PostTrigger: delete');

        $this->syncElasticSearch($id);
        return $response;
    }

    public function deleteWhere(array $where)
    {
        $primaryKey = $this->model->getKeyName();
        $ids = $this->onlyField($primaryKey, $where);
        $response = parent::deleteWhere($where);
        app('log')->debug('PostTrigger: deleteWhere');

        if (!empty($ids)) {
            $this->syncElasticSearch($ids);
        }
        return $response;
    }
}
