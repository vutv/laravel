<?php

namespace App\Trigger;

use Modules\Backend\Repositories\UserProfileRepository;

trait UserTrigger
{
    public function create(array $attributes)
    {
        $response = parent::create($attributes);

        $params = [
            'user_id' => $response['data']['id'],
            'name' => $response['data']['name'],
        ];
        app(UserProfileRepository::class)->add($params);

        app('log')->debug('-------- UserTrigger[create] ----------');
        app('log')->debug(var_export($params, true));

        return $response;
    }
}
