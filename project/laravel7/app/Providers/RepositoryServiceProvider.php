<?php

namespace App\Providers;

use App\Repositories\ES\PostElasticRepository;
use App\Repositories\ES\PostElasticRepositoryElastic;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind(PostElasticRepository::class, PostElasticRepositoryElastic::class);
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //:end-bindings:
    }
}
