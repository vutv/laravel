<?php

namespace App\Http\Controllers;

use Dingo\Api\Http\Response;
use Dingo\Api\Routing\Helpers;
use Illuminate\Routing\Controller;

class APIController extends Controller
{
    use Helpers;

    /**
     * @var array|\Illuminate\Http\Request|string
     */
    protected $request;

    /**
     * @var \Illuminate\Contracts\Foundation\Application|\Illuminate\Log\LogManager|mixed
     */
    protected $log;

    public function __construct()
    {
        $this->request = request();
        $this->log = app('log');
    }

    /**
     * Created by ManNV
     * @return mixed
     */
    protected function authUser()
    {
        $user = $this->user;
        return $user->toArray();
    }

    protected function responseData($data = [])
    {
//        if ($this->request->get('debug') == 1 && config('app.env') == 'local') {
//            echo '<pre>' . print_r($data, true) . '</pre>';
//            return 'ok';
//        }
        return new Response($data, Response::HTTP_OK);
    }

    protected function responseError($message, $code = 404)
    {
        app('log')->debug('responseError: ' . $message);
        $this->response->error($message, $code);
    }

    protected function badRequest($message)
    {
        $this->response->errorBadRequest($message);
    }

    protected function submitFormError($field, $message)
    {
        $error = [$field => [$message]];
        return new Response([
            'errors' => $error,
            'status_code' => Response::HTTP_UNPROCESSABLE_ENTITY
        ], Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
