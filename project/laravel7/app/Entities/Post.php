<?php

namespace App\Entities;

/**
 * Class Post
 * @package App\Entities
 */
class Post extends MyEntity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'tag', 'title', 'content'];
}
