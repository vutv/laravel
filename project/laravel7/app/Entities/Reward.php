<?php

namespace App\Entities;

/**
 * Class Reward
 * @package App\Entities
 */
class Reward extends MyEntity
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'rewards';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['owner_id', 'name', 'point', 'status', 'start_time', 'end_time'];
}
