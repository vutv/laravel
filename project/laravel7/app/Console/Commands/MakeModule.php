<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class MakeModule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'm:module {name} {--path=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new module';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $path = $this->option('path');
        if (!empty($path)) {
            $namespace = str_replace('/', '\\', $path);
            config(['modules.namespace' => config('modules.namespace') . '\\'. $namespace]);
            config(['modules.paths.modules' => config('modules.paths.modules') . '/' . $path]);
        }

        $name = $this->argument('name');
        if (empty($name)) {
            $this->error('Please input module name');
            return;
        }
        $this->info('----- begin create module [' . $name . '] --------');
        $this->call('module:make', [
            'name' => [$name],
        ]);

        $this->warn('----- make RepositoryServiceProvider for module [' . $name . '] --------');
        $this->call('module:make-provider', [
            'name' => 'RepositoryServiceProvider',
            'module' => $name
        ]);

        $this->info('----- end create module [' . $name . '] --------');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    public function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the Module.'],
        ];
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            ['path', 'p', InputOption::VALUE_NONE, 'path to module generate', null],
        ];
    }
}
