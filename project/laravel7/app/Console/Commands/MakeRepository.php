<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class MakeRepository extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'm:repository';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new repository!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if ($this->confirm('Would you like to create a Entity? [y|N]')) {
            $this->call('m:entity', [
                'name'    => $this->argument('name'),
                '--fillable' => $this->option('fillable'),
                '--force'    => $this->option('force'),
                '--skip-migration' => true
            ]);
        }

        if ($this->confirm('Would you like to create a Presenter? [y|N]')) {
            $this->call('make:presenter', [
                'name'    => $this->argument('name'),
                '--force' => null,
            ]);
        }

        $path = $this->option('path');
        if (!empty($path)) {
            $namespace = str_replace('/', '\\', $path) . '\\';
            config(['repository.generator.rootNamespace' => $namespace]);
            config(['repository.generator.basePath' => $path]);
        }

        if ($this->confirm('Would you like to create a Controller? [y|N]')) {

            $resource_args = [
                'name'    => $this->argument('name')
            ];

            // Generate a controller resource
            $controller_command = ((float) app()->version() >= 5.5  ? 'make:rest-controller' : 'make:resource');
            $this->call($controller_command, $resource_args);
        }

        $this->call('make:repository', [
            'name'        => $this->argument('name'),
            '--fillable'  => null,
            '--rules'     => null,
            '--validator' => null,
            '--force'     => null,
            '--skip-model' => true
        ]);

        $this->call('make:bindings', [
            'name'    => $this->argument('name'),
            '--force' => null
        ]);
    }



    /**
     * The array of command arguments.
     *
     * @return array
     */
    public function getArguments()
    {
        return [
            [
                'name',
                InputArgument::REQUIRED,
                'The name of class being generated.',
                null
            ],
        ];
    }


    /**
     * The array of command options.
     *
     * @return array
     */
    public function getOptions()
    {
        return [
            [
                'fillable',
                null,
                InputOption::VALUE_OPTIONAL,
                'The fillable attributes.',
                null
            ],
            [
                'rules',
                null,
                InputOption::VALUE_OPTIONAL,
                'The rules of validation attributes.',
                null
            ],
            [
                'validator',
                null,
                InputOption::VALUE_OPTIONAL,
                'Adds validator reference to the repository.',
                null
            ],
            [
                'force',
                'f',
                InputOption::VALUE_NONE,
                'Force the creation if file already exists.',
                null
            ],
            [
                'path',
                'p',
                InputOption::VALUE_OPTIONAL,
                'Path for repository of entity.',
                null
            ],
        ];
    }
}
