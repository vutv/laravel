<?php

namespace App\Presenters;

use App\Transformers\RewardTransformer;

/**
 * Class RewardPresenter.
 *
 * @package namespace App\Presenters;
 */
class RewardPresenter extends MyPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new RewardTransformer();
    }
}
