<?php

namespace App\Presenters;

use App\Transformers\PostTransformer;

/**
 * Class PostPresenter.
 *
 * @package namespace App\Presenters;
 */
class PostPresenter extends MyPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PostTransformer();
    }
}
