<?php
return [
    'hosts' => [env('ELASTICSEARCH_HOST', 'elasticsearch:9200')],
    'post' => [
        'index' => env('ELASTICSEARCH_POST_INDEX', 'base_post'),
        'type' => 'post'
    ],
];
