import Vue from 'vue'
import CommonAPI from '~/services/CommonAPI'
import Auth from '~/services/Auth'

export default function ({ $axios, store }) {
  const log = Vue.prototype.$log
  const services = {
    common: new CommonAPI($axios, log, '/'),
    // applicant: new ApplicantAPI($axios, log, '/applicant/'),
  }

  Vue.prototype.$services = services
  Vue.prototype.$authInfo = new Auth(store)
}
