export default {
  methods: {
    validatePhoneNumber() {
      return [
        { type: 'string' },
        {
          required: true,
          whitespace: true,
          message: this.$t(`Please input phone number`),
        },
        { pattern: /^[0-9]*$/, message: this.$t(`Please enter the number`) },
        {
          min: 10,
          max: 15,
          message: this.$t(
            `Phone number must be between {min} and {max} number`,
            { min: 10, max: 15 }
          ),
        },
      ]
    },
    validateEmailAddress() {
      return [
        { type: 'string' },
        {
          required: true,
          whitespace: true,
          message: this.$t(`Please input email`),
        },
        {
          pattern: /^[-a-z0-9~!$%^&*_=+}{\\'?]+(\.[-a-z0-9~!$%^&*_=+}{\\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i,
          message: this.$t(`Email is not a valid email address`),
        },
      ]
    },
  },
}
