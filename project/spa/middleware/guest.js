import Vue from 'vue'

export default function ({ redirect }) {
  const $authInfo = Vue.prototype.$authInfo
  if ($authInfo.checkAuth()) {
    // eslint-disable-next-line eqeqeq
    // if ($authInfo.role() == 1) {
    //   return redirect({ name: '' })
    // }
    return redirect({ name: 'index' })
  }
}
