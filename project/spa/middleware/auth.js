import Vue from 'vue'

export default function ({ redirect, route }) {
  const $authInfo = Vue.prototype.$authInfo
  // console.log(this.$cookies.get('access_token'))
  // console.log($authInfo)
  if (!$authInfo.checkAuth()) {
    return redirect({
      name: 'login',
      query: { continue: route.fullPath },
    })
  }
}
