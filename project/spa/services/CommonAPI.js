import BaseService from './BaseService'

export default class CommonAPI extends BaseService {
  async demoCommon(success, error) {
    await this.get('common', success, error)
  }

  async getUserInfo(sessionId, rolePermission, success, error) {
    await this.get(
      'user-info/?session_id=' + sessionId + '&role=' + rolePermission,
      success,
      error
    )
  }

  async login(params, success, error) {
    await this.post('login', params, success, error)
  }

  async forgetPassword(params, success, error) {
    await this.post('password/forget', params, success, error)
  }

  async resetPasswordVerifyURL(params, success, error) {
    await this.post('password/verify', params, success, error)
  }

  async resetPassword(params, success, error) {
    await this.post('password/reset', params, success, error)
  }

  async createReward(param, success, error) {
    await this.post('common/reward/create', param, success, error)
  }

  async getListReward(param, success, error) {
    await this.get('common/reward?' + this.urlParse(param), success, error)
  }

  async removeReward(id, success, error) {
    await this.post('/reward/' + id + '/delete', null, success, error)
  }

  async createPost(param, success, error) {
    await this.post('common/post/create', param, success, error)
  }

  async getListPost(param, success, error) {
    await this.get('common/post?' + this.urlParse(param), success, error)
  }

  async removePost(id, success, error) {
    await this.post('/post/' + id + '/delete', null, success, error)
  }
}
