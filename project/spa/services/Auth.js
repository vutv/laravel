export default class Auth {
  constructor(store) {
    this.data = null
    this.store = store
    if (store.state.user.data != {}) {
      this.data = store.state.user.data
    }
  }

  checkAuth() {
    return this.id() != null
  }

  id() {
    return this.getData('id')
  }

  name() {
    return this.getData('name')
  }

  avatar() {
    if (this.isCompany()) {
      return this.getData('company')['logo']
    }

    return this.profile()['avatar']
  }

  email() {
    return this.getData('email')
  }

  role() {
    return this.getData('role')
  }
  profile() {
    return this.getData('profile')
  }

  token() {
    return this.getData('access_token')
  }

  getData(field) {
    if (this.data == null) {
      return null
    }
    // eslint-disable-next-line no-prototype-builtins
    if (this.data.hasOwnProperty(field)) {
      return this.data[field]
    }
    return null
  }

  loginAction(response) {
    this.store.commit('user/saveUserInfo', response)
    this.data = response
  }

  logoutAction() {
    this.store.commit('user/logoutAction')
    this.data = null
  }
}
