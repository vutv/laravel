export const strict = false

const syncObject = {
  commit: null,
  context: null,
}

export const actions = {
  async nuxtServerInit({ commit }) {
    syncObject.commit = commit
    syncObject.context = this
    //get config
    // await syncObject.getConfig()
    // //end get config
    //
    // //begin get user info
    // await syncObject.getUserInfo()
    // //end begin get user info
  },
}
//
// import Vue from 'vue'
// import Cookies from 'js-cookie'
//
// import { cookieFromRequest } from '~/utils'
//
// export const actions = {
//   nuxtServerInit ({ commit }, { req }) {
//     const token = cookieFromRequest(req, 'token')
//     if (token) {
//       commit('auth/SET_TOKEN', token)
//     }
//
//     const locale = cookieFromRequest(req, 'locale')
//     if (locale) {
//       commit('lang/SET_LOCALE', { locale })
//     }
//   },
//
//   nuxtClientInit ({ commit }) {
//     const token = Cookies.get('token')
//     if (token) {
//       commit('auth/SET_TOKEN', token)
//     }
//
//     const locale = Cookies.get('locale')
//     if (locale) {
//       commit('lang/SET_LOCALE', { locale })
//     }
//   }
// }
