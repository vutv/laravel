require('dotenv').config()
export default {
  // Disable server-side rendering (https://go.nuxtjs.dev/ssr-mode)
  ssr: false,

  server: {
    port: process.env.APP_PORT, // default: 3000
    host: '0.0.0.0', // default: localhost
  },

  // // rewrite URL
  // router: {
  //   extendRoutes(routes, resolve) {
  //     routes.push({
  //       name: 'exam-detail',
  //       path: '/exam/:id([a-zA-Z0-9]+)',
  //       component: resolve(__dirname, 'pages/participants/exam/_id.vue'),
  //     })
  //     routes.push({
  //       name: 'exam-do',
  //       path: '/exam/:id([a-zA-Z0-9]+)/do',
  //       component: resolve(__dirname, 'pages/participants/exam/do.vue'),
  //     })
  //     routes.push({
  //       name: 'exam-result',
  //       path: '/exam/:id([a-zA-Z0-9]+)/result',
  //       component: resolve(__dirname, 'pages/participants/exam/result.vue'),
  //     })
  //     routes.push({
  //       name: 'attack-question',
  //       path: '/examiner/exam/:id([a-zA-Z0-9]+)/question',
  //       component: resolve(__dirname, 'pages/examiner/exam/question.vue'),
  //     })
  //     routes.push({
  //       name: '/',
  //       path: '/',
  //       component: resolve(__dirname, 'pages/participants'),
  //     })
  //   },
  // },

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: process.env.APP_NAME || 'spa',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.APP_DESCRIPTION || '',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: ['element-ui/lib/theme-chalk/index.css'],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    '~/plugins/consola',
    '~/plugins/axios',
    '~/plugins/services',
    '~/plugins/element-ui',
    '~/plugins/mixin',
    '~/plugins/component',
    '~/plugins/i18n',
    { src: '~/plugins/swiper', ssr: false },
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/dotenv',
    'bootstrap-vue/nuxt',
    '@nuxtjs/style-resources',
  ],

  styleResources: {
    scss: [
      './assets/scss/mixin/main-mixin.scss',
      './assets/scss/variables.scss',
    ],
  },

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
    baseURL: process.env.URL_API + '/api',
    // baseURL: 'http://local.testbank.com/api',
    https: true,
    // proxy: true,
    // debug: 'debug',
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        })
      }
    },
  },
}
